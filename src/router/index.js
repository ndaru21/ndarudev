import {createRouter, createWebHistory} from 'vue-router'

const routes = [
    {
        path: '/:pathMatch(.*)*',
        name: 'not-found',
        component: () => import( /* webpackChunkName: "404" */ '@/components/errors/404.vue')
    },
    {
        path: '/',
        name: 'Home',
        component: () => import(/* webpackChunkName: "Home" */ '@/views/Home.vue')
    },
    {
        path: '/webdesign',
        name: 'WebDesign',
        component: () => import(/* webpackChunkName: "WebDesign" */ '@/views/WebDesign.vue')
    }
]

const router = createRouter({
    history: createWebHistory(),
    routes
})

export default router