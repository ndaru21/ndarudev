const superiorities = {
    webDesign: [{
            title: 'Bebas Konsultasi',
            desc: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit quos.',
            style: 'background: rgb(193,232,198); background: linear-gradient(143deg, rgba(193,232,198,1) 37%, rgba(138,208,167,1) 100%);'
        },
        {
            title: 'Harga Terjangkau',
            desc: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit quos.',
            style: 'background: rgb(241,184,179); background: linear-gradient(227deg, rgba(241,184,179,1) 37%, rgba(215,119,109,1) 100%);'
        },
        {
            title: 'Bisa Nego',
            desc: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit quos.',
            style: 'background: rgb(168,212,241); background: linear-gradient(171deg, rgba(168,212,241,1) 37%, rgba(105,170,213,1) 100%);'
        },
        {
            title: 'Desain Gaul',
            desc: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit quos.',
            style: 'background: rgb(226,190,240); background: linear-gradient(54deg, rgba(226,190,240,1) 37%, rgba(195,128,223,1) 100%);'
        },
        {
            title: 'Proses Cepat',
            desc: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit quos.',
            style: 'background: rgb(255,229,185); background: linear-gradient(141deg, rgba(255,229,185,1) 37%, rgba(255,205,121,1) 100%);'
        }
    ]
}

export default superiorities