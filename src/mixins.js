const Mixins = {
    methods: {
        getImgUrl(url) {
            return new URL(`/src/assets/images/${url}`, import.meta.url).href
        }
    }
}

export default Mixins