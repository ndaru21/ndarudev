import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

// Aos
import AOS from 'aos';
import 'aos/dist/aos.css';
AOS.init();

// Bootstrap & Popper
import "bootstrap/dist/css/bootstrap.min.css"
import "bootstrap"
import "@popperjs/core"

createApp(App).use(router).use(store).mount('#app')
