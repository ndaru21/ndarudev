const faqs = {
    webDesign: [{
            question: 'Dengan apa saja saya bisa membayar?',
            answer: 'Pembayaran dilakukan bisa lewat bank transfer dan juga E-wallet. Untuk E-wallet kita hanya ada Gopay, Dana, Link Aja dan Ovo. Untuk bank transfer kita ada BCA dan Jago.'
        },
        {
            question: 'Berapa lamakah konfirmasi pembayaran dilakukan?',
            answer: 'Paling cepat 8 detik, paling lama 1 jam. Jadi jika tidak ingin menunggu lama. Anda bisa chat di whatsapp sebelum melakukan pembelian.'
        },
        {
            question: 'Saya berada diluar indonesia?',
            answer: 'Jika Anda bukanlah orang indonesia, tetapi ingin tetap berlangganan dengan Parsinta. Maka Anda bisa membayarnya melalui Wise.'
        }
    ]
}

export default faqs