const testimonials = {
    webDesign: [{
            picture: '',
            name: 'Muhamad Ndaru',
            rating: 5,
            caption: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit nihil temporibus ex magni dolores ratione ullam voluptas.',
        },
        {
            picture: '',
            name: 'Joko Widodo',
            rating: 4,
            caption: 'Gokil banget sih ini, website gue langsung peringkat 1 google pake SEO Agency dari RuApps.',
        },
        {
            picture: '',
            name: 'Arya Javas Fatih',
            rating: 4,
            caption: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit nihil temporibus ex magni dolores ratione ullam voluptas.',
        },
        {
            picture: '',
            name: 'Anggie Dwi Putra',
            rating: 5,
            caption: 'Keren banget sekarang bisnis saya sudah mempunyai website, Terima kasih RuApps, mantapp pokoknya.',
        },
        {
            picture: '',
            name: 'Rafa Adila',
            rating: 5,
            caption: 'Mantapp nih RuApps, biayanya terjangkau dan hasilnya memuaskan, semoga bisnis saya bisa lebih maju.',
        },
    ]
}

export default testimonials