const plans = {
    oneTime: [{
            title: 'Starter',
            description: 'Paket murmer untuk memulai bangun web',
            // oldPrice: '1.8Jt',
            currPrice: '1.5Jt',
            features: [
                'Gratis Domain .my.id', 'Web Hosting (1 Tahun)', 'Desain Premium', 'Maksimal 3 Halaman', 'Revisi Konten 2x', 'Gratis Konsultasi', 'Satu Hari Kerja'
            ]
        }, {
            title: 'Premium',
            description: 'Paket murmer untuk memulai bangun web',
            oldPrice: '2.9Jt',
            currPrice: '2.2Jt',
            features: [
                'Gratis Domain .my.id', 'Web Hosting (1 Tahun)', 'Desain Premium', 'Maksimal 3 Halaman', 'Revisi Konten 2x', 'Gratis Konsultasi', 'Satu Hari Kerja'
            ]
        },
        {
            title: 'Inferno',
            description: 'Paket murmer untuk memulai bangun web',
            oldPrice: '4Jt',
            currPrice: '3.3Jt',
            features: [
                'Gratis Domain .my.id', 'Web Hosting (1 Tahun)', 'Desain Premium', 'Maksimal 3 Halaman', 'Revisi Konten 2x', 'Gratis Konsultasi', 'Satu Hari Kerja'
            ]
        },
        {
            title: 'Expert Custom',
            description: 'Paket murmer untuk memulai bangun web',
            oldPrice: '1000k',
            currPrice: '899k',
            features: [
                'Gratis Domain .my.id', 'Web Hosting (1 Tahun)', 'Desain Premium', 'Maksimal 3 Halaman', 'Revisi Konten 2x', 'Gratis Konsultasi', 'Satu Hari Kerja'
            ]
        }
    ],
    monthly: [{}]
}

export default plans